# seal
Watch out for **SEAL**: a **SE**ction **AL**ignment machine learning model for Wikipedia articles.

Pass a section title in a source language and you'll get the equivalent one in other languages.

## Trigger an Airflow test run

Follow this walkthrough to simulate a production execution of the
pipeline on your stat box. Inspired by [this
snippet](https://gitlab.wikimedia.org/-/snippets/71).

### Build your artifact

1.  Pick a branch you want to test from the drop-down menu
2.  Click on the pipeline status button, it should be a green tick
3.  Click on the *play* button next to `package`, wait until done
4.  Go to the [Package Registry](https://gitlab.wikimedia.org/repos/structured-data/seal/-/packages)
5.  Click on the first item in the list, then copy the Asset URL. It
    should be something like
    `https://gitlab.wikimedia.org/repos/structured-data/seal/-/package_files/1526/download`

### Get your artifact ready

``` sh
me@stat1008:~$ mkdir artifacts
me@stat1008:~$ cd artifacts
me@stat1008:~$ wget -O MY_ARTIFACT MY_COPIED_ASSET_URL
me@stat1008:~$ hdfs dfs -mkdir artifacts
me@stat1008:~$ hdfs dfs -copyFromLocal MY_ARTIFACT artifacts
me@stat1008:~$ hdfs dfs -chmod -R o+rx artifacts
```

### Spin up an Airflow instance

On your stat box:

``` sh
me@stat1008:~$ git clone https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags.git
me@stat1008:~$ cd airflow-dags
me@stat1008:~$ rm -r /tmp/MY_AIRFLOW_HOME  # If you've previously run the next command
me@stat1008:~$ sudo -u analytics-privatedata ./run_dev_instance.sh -m /tmp/MY_AIRFLOW_HOME -p MY_PORT platform_eng
```

On your local box:

``` sh
me@my_box:~$ ssh -t -N stat1008.eqiad.wmnet -L MY_PORT:stat1008.eqiad.wmnet:MY_PORT
```

### Trigger the DAG run

1.  Go to `http://localhost:MY_PORT/` on your browser
2.  On the top bar, go to **Admin \> Variables**
3.  Click on the middle button (*Edit record*) next to the
    `platform_eng/dags/seal_dag.py` Key
4.  Update `{ "conda_env" : "hdfs://analytics-hadoop/user/ME/artifacts/MY_ARTIFACT" }`
5.  Add any other relevant DAG properties
6.  Click on the *Save* button
7.  On the top bar, go to **DAGs** and click on the `seal`
    slider. This should trigger an automatic DAG run
8.  Click on `seal`

You're all set!

## Release

1.  Click on the pipeline status button, it should be a green tick
2.  Click on the *play* button next to `release`
3.  If the job went fine, you'll find a new artifact in the [Package
    Registry](https://gitlab.wikimedia.org/repos/structured-data/seal/-/packages)

We follow Data Engineering's
[workflow_utils](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md#project-versioning): -
the `main` branch is on a `.dev` release - releases are made by removing
the `.dev` suffix and committing a tag

## Deploy

1.  Click on the pipeline status button, it should be a green tick
2.  Click on the *play* button next to `bump_on_airflow_dags`. This
    will create a merge request at
    [airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags)
3.  Double-check it and merge
4.  Deploy the DAGs:

``` sh
me@my_box:~$ ssh deployment.eqiad.wmnet
me@deploy1002:~$ cd /srv/deployment/airflow-dags/platform_eng/
me@deploy1002:~$ git pull
me@deploy1002:~$ scap deploy
```

See the
[docs](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md#example-pipeline-usage)
for more details.
