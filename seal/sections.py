#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Gather relevant content from Wikipedia sections."""

import argparse
import re
import string

from typing import Pattern

import mwparserfromhell as mwp

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

from seal import constants


# ASCII punctuation to be trimmed from section headings
PUNCTUATION = string.punctuation.replace('()', ' ')
WHITESPACE_RE = re.compile(r'\s')
SECTIONS_SCHEMA = T.ArrayType(
    T.StructType(
        [
            T.StructField('heading', T.StringType()),
            T.StructField('links', T.ArrayType(T.StringType())),
            T.StructField('pos_start', T.IntegerType()),
            T.StructField('pos_end', T.IntegerType()),
        ]
    )
)


def get_articles(
    spark: SparkSession, wiki_dbs: str, monthly_snapshot: str, weekly_snapshot: str
) -> DataFrame:
    """Return all articles for lang in the given snapshot with their wikidata items."""

    wikitext_df = spark.sql(
        f"""SELECT page_id, page_title, wiki_db, revision_text
    FROM wmf.mediawiki_wikitext_current
    WHERE snapshot = '{monthly_snapshot}' and wiki_db in ({wiki_dbs}) and page_namespace = 0
    """
    )
    ipl_df = spark.sql(
        f"""SELECT item_id, page_id, wiki_db
    FROM wmf.wikidata_item_page_link
    WHERE snapshot = '{weekly_snapshot}' and wiki_db in ({wiki_dbs}) and page_namespace = 0
    """
    )
    articles_df = wikitext_df.join(ipl_df, ['page_id', 'wiki_db'], how='inner').select(
        'item_id',
        'wiki_db',
        'page_title',
        'revision_text',
    )
    return articles_df


@F.udf(returnType=SECTIONS_SCHEMA)
def parse_wikitext(
    wikitext: str, sub_re: Pattern = WHITESPACE_RE, strip_chars: str = PUNCTUATION
) -> F.udf:
    sections = mwp.parse(wikitext).get_sections(levels=[2], include_headings=True)
    total_sections = len(sections)
    parsed_sections = []
    for i, section in enumerate(sections):
        try:
            section_links = [link.title.strip_code() for link in section.filter_wikilinks()]
            section = section.strip_code()
            section_heading, *_ = section.split('\n', maxsplit=1)
            section_heading = sub_re.sub(' ', section_heading).strip(strip_chars)
            parsed_sections.append(
                dict(
                    heading=section_heading,
                    links=section_links,
                    pos_start=i + 1,
                    pos_end=total_sections - i,
                )
            )
        except KeyError:  # when mwp is unable to convert an invalid html entity to unicode point
            continue
    return parsed_sections


def extract_section_attributes(articles_df: DataFrame) -> DataFrame:
    """Extract section headings and links for each article
    in `articles_df`, then align the links to their wikidata QIDs.

    Return a dataframe containing array of structs {section_heading,
    section_links} for each article.
    """
    # apply udf to get section heading and links from revision text
    sections_df = articles_df.select(
        'item_id', 'wiki_db', F.explode(parse_wikitext('revision_text')).alias('section')
    )
    sections_df = sections_df.select('item_id', 'wiki_db', F.col('section.*'))
    sections_df = sections_df.withColumn('heading', F.lower(F.col('heading')))
    sections_df = sections_df.cache()

    sections_df.count()

    counts_df = sections_df.groupBy(['heading', 'wiki_db']).agg(
        F.count('*').alias('count'),
        F.mean('pos_start').alias('pos_start_mean'),
        F.mean('pos_end').alias('pos_end_mean'),
    )
    headings_match = (F.col('sections.heading') == F.col('counts.heading')) & (
        F.col('sections.wiki_db') == F.col('counts.wiki_db')
    )
    section_attrs_df = (
        sections_df.alias('sections')
        .join(counts_df.alias('counts'), headings_match)
        .select('sections.*', 'counts.count', 'counts.pos_start_mean', 'counts.pos_end_mean')
    )
    section_attrs_df = section_attrs_df.withColumn('link', F.explode_outer('links'))
    page_titles_df = articles_df.select('page_title', 'item_id', 'wiki_db')
    # align each link with its wikidata id
    section_attrs_df = (
        section_attrs_df.alias('sections')
        .join(
            page_titles_df.alias('page'),
            (
                (F.col('sections.link') == F.col('page.page_title'))
                & (F.col('sections.wiki_db') == F.col('page.wiki_db'))
            ),
            how='left',
        )
        .select(
            'sections.item_id',
            'sections.wiki_db',
            'heading',
            'pos_start_mean',
            'pos_end_mean',
            'count',
            F.col('page.item_id').alias('link'),
        )
    )
    section_attrs_df = section_attrs_df.groupBy(
        ['item_id', 'heading', 'wiki_db', 'count', 'pos_start_mean', 'pos_end_mean']
    ).agg(F.collect_set('link').alias('links'))
    # pack each articles section attrs first into a struct and then all into an array
    section_attrs_df = section_attrs_df.groupBy(['item_id', 'wiki_db']).agg(
        F.collect_list(
            F.struct(
                F.col('heading'),
                F.col('links'),
                F.col('count'),
                F.col('pos_start_mean'),
                F.col('pos_end_mean'),
            )
        ).alias('section_attributes')
    )
    return section_attrs_df


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(description='Gather relevant content from Wikipedia sections')

    # Required / positional
    parser.add_argument('weekly_snapshot', metavar='YYYY-MM-DD', help='weekly snapshot date')
    parser.add_argument('monthly_snapshot', metavar='YYYY-MM', help='monthly snapshot date')

    # Optional
    parser.add_argument(
        '-w',
        '--work-dir',
        metavar='/hdfs_path/to/dir/',
        default=constants.WORK_DIR,
        help=f'Absolute HDFS path to the working directory. Default: "{constants.WORK_DIR}" in the current user home',
    )
    parser.add_argument(
        '-c',
        '--coalesce',
        metavar='N',
        default=constants.COALESCE,
        help=(
            'Control the amount of files per output partition. '
            'A higher value implies more files but a faster and lighter execution. '
            f'Default: {constants.COALESCE}'
        ),
    )

    return parser.parse_args()


def main(args: argparse.Namespace, spark: SparkSession) -> None:  # pragma: no cover
    weekly_snapshot = args.weekly_snapshot
    monthly_snapshot = args.monthly_snapshot
    work_dir = args.work_dir
    coalesce = args.coalesce

    training_path = constants.TRAINING_PATH.format(work_dir=work_dir)
    sections_path = constants.SECTIONS_PATH.format(work_dir=work_dir, snapshot=weekly_snapshot)

    cx_sources = spark.read.json(f'{training_path}/content_translation_sources.json').collect()
    langs = [s['wp-code'] for s in cx_sources]
    wiki_db = ', '.join([f"'{lang.replace('-', '_')}wiki'" for lang in langs])

    articles_df = get_articles(spark, wiki_db, monthly_snapshot, weekly_snapshot)
    section_attrs_df = extract_section_attributes(articles_df)

    section_attrs_df.coalesce(coalesce).write.parquet(sections_path, mode='overwrite')


if __name__ == '__main__':
    args = parse_args()

    # Uncomment for prod
    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_custom_session

    # spark = create_custom_session(
    # app_name='seal_sections',
    # master='yarn',
    # ship_python_env=True,
    # spark_config={
    # 'spark.driver.memory': '4g',
    # 'spark.executor.memory': '8g',
    # 'spark.executor.cores': 4,
    # "spark.dynamicAllocation.maxExecutors": 128,
    # 'spark.sql.shuffle.partitions': 2048,
    # 'spark.reducer.maxReqsInFlight': 1,
    # 'spark.shuffle.io.retryWait': '60s',
    # },
    # )
    main(args, spark)
    spark.stop()
