#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Train & evaluate a Wikipedia section alignment model, then
predict all *(source, target)* heading pairs.
"""

import argparse

from math import sqrt
from typing import List, Tuple

import pandas as pd

from pyspark.sql import DataFrame as SparkDataFrame
from pyspark.sql import SparkSession, Window
from pyspark.sql import functions as F
from pyspark.sql import types as T
from pyspark.sql.utils import AnalysisException
from xgboost import XGBClassifier

from seal import constants


FEATURE_COLS = [
    'heading_similarity',
    'link_similarity_mean',
    'link_similarity_sum',
    'co-occurrence_count_norm',
    'source_count',
    'target_count',
    'source_pos_start',
    'target_pos_start',
    'source_pos_end',
    'target_pos_end',
    'edit_distance',
]
RANDOM_SEED = 1984
PROBABILITY_THRESHOLD = 0.9


def preprocess_cx_data(df: SparkDataFrame, n: int = 5) -> SparkDataFrame:
    """Transform the dataframe into a form that is compatible with
    the data extracted from the pipeline.
    """
    df = (
        df.withColumn('source', F.lower(F.col('source_title')))
        .withColumn('target', F.lower(F.col('target_title')))
        .withColumn('source_language', F.concat(F.col('source_language'), F.lit('wiki')))
        .withColumn('target_language', F.concat(F.col('target_language'), F.lit('wiki')))
    )
    # group identical pairs and sum frequency so that there are no duplicates
    df = df.groupBy('source', 'target', 'source_language', 'target_language').agg(
        F.sum('frequency').alias('frequency')
    )
    df = df.filter(F.col('frequency') > n)
    return df


def make_data_df(ground_truth_df: SparkDataFrame, features_df: SparkDataFrame) -> SparkDataFrame:
    """Return a dataset with all possible targets for each (source, source_language,
    target_language) in ground_truth_df. Labels all pairs occurring in the ground truth
    as 1 and the rest as 0.
    """
    cols = ['source', 'source_language', 'target_language']
    # All pairs with (source, source_language, target_language) in ground truth
    relevant_pairs_df = features_df.join(ground_truth_df, on=cols, how='left_semi')

    # All relevant pairs with (source, target, source_language, target_language) in ground truth
    in_gt_df = relevant_pairs_df.join(ground_truth_df, on=[*cols, 'target'], how='left_semi')
    in_gt_df = in_gt_df.withColumn('label', F.lit(1))

    # All relevant pairs whose target does not occur in ground truth
    not_in_gt_df = relevant_pairs_df.join(ground_truth_df, on=[*cols, 'target'], how='left_anti')
    not_in_gt_df = not_in_gt_df.withColumn('label', F.lit(0))

    return in_gt_df.union(not_in_gt_df).distinct()


def fit(
    train_df: pd.DataFrame, test_df: pd.DataFrame, precision_at: int = 5
) -> Tuple[XGBClassifier, float]:
    """Fit a :class:`xgboost.XGBClassifier`."""
    X_train, X_test = train_df[FEATURE_COLS], test_df[FEATURE_COLS]
    y_train = train_df['label']
    neg_pos_ratio = len(train_df[train_df['label'] == 0]) / len(train_df[train_df['label'] == 1])
    classifier = XGBClassifier(
        scale_pos_weight=sqrt(neg_pos_ratio),
        use_label_encoder=False,
        tree_method='hist',
        learning_rate=0.1,
        random_state=RANDOM_SEED,
    )
    classifier.fit(X_train, y_train)

    y_pred = classifier.predict_proba(X_test)
    test_df['prediction'] = y_pred[:, 1]

    source_grouper = test_df.groupby(['source', 'source_language', 'target_language'])
    test_df = test_df[(source_grouper['label'].transform(lambda x: x.eq(1).any()))]
    test_df = (
        test_df.sort_values(by=['prediction', 'heading_similarity'], ascending=False)
        .groupby(['source', 'source_language', 'target_language'])
        .head(precision_at)
    )
    test_df['within_precision'] = test_df.groupby(
        ['source', 'source_language', 'target_language']
    )['label'].transform(max)
    precision = len(test_df[test_df['within_precision'] == 1]) / len(test_df)

    return classifier, precision


def predict(model: XGBClassifier, schema: T.StructType, res_cols: List[str]) -> F.pandas_udf:
    @F.pandas_udf(schema, F.PandasUDFType.GROUPED_MAP)
    def predict_inner(df: pd.DataFrame) -> pd.DataFrame:
        y_pred = model.predict_proba(df[FEATURE_COLS])
        df['probability'] = y_pred[:, 1]
        return df[res_cols]

    return predict_inner


def get_precision_at_k(predictions_df: SparkDataFrame, k: int = 5) -> SparkDataFrame:
    cols = ['source', 'source_language', 'target_language']
    window = Window.partitionBy(cols).orderBy(F.col('probability').desc())
    k_df = predictions_df.select('*', F.rank().over(window).alias('rank')).filter(
        F.col('rank') <= k
    )
    precision_df = k_df.groupBy(cols).agg(F.max('label').alias('label'))
    precision_df = precision_df.groupBy('source_language', 'target_language').agg(
        (F.count(F.when(F.col('label') == 1, F.lit(1))) / F.count('*')).alias('precision'),
        F.count('*').alias('matched_pairs'),
    )
    return precision_df


def train(spark: SparkSession, features_path: str, training_path: str) -> XGBClassifier:
    """Build the training data and train a :class:`xgboost.XGBClassifier`."""
    # Languages for which we have annotated data
    langs = ['en', 'es', 'ar', 'ja', 'fr', 'ru']
    features_files = [f'{features_path}/{lang}wiki' for lang in langs]
    features_df = spark.read.parquet(*features_files)
    ground_truth_df = spark.read.parquet(f'{training_path}/ground_truth')
    ground_truth_df = ground_truth_df.select(
        F.lower(F.col('source')).alias('source'),
        F.lower(F.col('target')).alias('target'),
        'source_language',
        'target_language',
    )

    data_df = make_data_df(ground_truth_df, features_df)
    join_cols = ['source', 'source_language', 'target_language']
    unique_section_pairs = ground_truth_df.drop_duplicates(join_cols)

    train_split, test_split = unique_section_pairs.randomSplit([0.8, 0.2], seed=RANDOM_SEED)
    # NOTE The following 2 lines may hit `SparkException` due to a timed-out broadcast
    train_df = data_df.join(train_split, on=join_cols, how='left_semi').toPandas()
    test_df = data_df.join(test_split, on=join_cols, how='left_semi').toPandas()
    model, precision = fit(train_df, test_df)
    print(f'\nTraining complete. Precision: {precision:.3f}\n')

    return model


def evaluate(
    model: XGBClassifier, spark: SparkSession, features_path: str, training_path: str
) -> SparkDataFrame:
    # Evaluation against CX data
    features_files = f'{features_path}/*'
    features_df = spark.read.parquet(features_files)
    # TODO this should come from the CX tool: can a fresh version be pulled?
    cx_path = f'{training_path}/evaluation_set'
    cx_df = spark.read.parquet(cx_path)

    cx_df = preprocess_cx_data(cx_df, 0)
    cx_data_df = make_data_df(cx_df, features_df)
    sources_df = cx_data_df.groupBy('source', 'source_language', 'target_language').agg(
        F.max('label').alias('label')
    )
    frequency_df = cx_df.groupBy('source', 'source_language', 'target_language').agg(
        F.sum(F.col('frequency')).alias('frequency')
    )
    source_count_df = (
        sources_df.join(
            frequency_df,
            on=['source', 'source_language', 'target_language'],
        )
        .filter(~((F.col('label') == 0) & (F.col('frequency') < 3)))
        .groupBy('source_language', 'target_language')
        .count()
    )
    available_sources_df = sources_df.filter(F.col('label') == 1)
    test_schema = T.StructType(
        [
            T.StructField('source', T.StringType()),
            T.StructField('target', T.StringType()),
            T.StructField('source_language', T.StringType()),
            T.StructField('target_language', T.StringType()),
            T.StructField('probability', T.FloatType()),
            T.StructField('label', T.IntegerType()),
        ]
    )
    test_cols = ['source', 'target', 'source_language', 'target_language', 'probability', 'label']
    # Rank section translations using pre-trained model
    predictions_df = (
        cx_data_df.join(available_sources_df, on=['source', 'target_language'], how='left_semi')
        .groupby('source', 'source_language', 'target_language')
        .apply(predict(model, test_schema, test_cols))
    )
    # Get precision @ 5 for all source language, target language pairs
    precision_df = (
        get_precision_at_k(predictions_df)
        .alias('p')
        .join(
            source_count_df.alias('s'),
            on=['source_language', 'target_language'],
            how='left',
        )
        .select('p.*', F.col('s.count').alias('total_pairs'))
    )
    # Polish
    precision_df = (
        precision_df.dropna()
        .select(
            'source_language',
            'target_language',
            F.round('precision', 3),
            'matched_pairs',
            F.col('total_pairs').cast('int'),
        )
        .orderBy('matched_pairs', ascending=False)
    )

    return precision_df


def predict_all(
    model: XGBClassifier,
    spark: SparkSession,
    training_path: str,
    features_path: str,
    alignments_path: str,
    probability_threshold: float,
) -> SparkDataFrame:
    """Build the output dataset.

    Predict all *(source, target)* section heading pairs, rank the top 20,
    and keep those above a probability threshold.
    """
    cx_sources = spark.read.json(f'{training_path}/content_translation_sources.json').collect()
    langs = [f"{source['wp-code'].replace('-', '_')}wiki" for source in cx_sources]
    rank_schema = T.StructType(
        [
            T.StructField('source', T.StringType()),
            T.StructField('target', T.StringType()),
            T.StructField('source_language', T.StringType()),
            T.StructField('target_language', T.StringType()),
            T.StructField('probability', T.FloatType()),
        ]
    )
    rank_cols = ['source', 'target', 'source_language', 'target_language', 'probability']

    # Same as above, plus the `rank` column.
    # Redundant, but it seems like there's no other way.
    output_schema = T.StructType(
        [
            T.StructField('source', T.StringType()),
            T.StructField('target', T.StringType()),
            T.StructField('source_language', T.StringType()),
            T.StructField('target_language', T.StringType()),
            T.StructField('probability', T.FloatType()),
            T.StructField('rank', T.IntegerType()),
        ]
    )
    output = spark.createDataFrame([], output_schema)

    for lang in langs:
        try:
            features_df = spark.read.parquet(f'{features_path}/{lang}')
        except AnalysisException:
            continue

        # Predict
        prediction_df = (
            features_df.withColumn('id', F.lit(F.rand() * 2048 % 2048).cast('int'))
            .groupby('id')
            .apply(predict(model, rank_schema, rank_cols))
        )
        # Get the top 20 predictions for each source section
        window = Window.partitionBy('source', 'source_language', 'target_language').orderBy(
            F.col('probability').desc()
        )
        k_df = prediction_df.select('*', F.row_number().over(window).alias('rank')).filter(
            F.col('rank') <= 20
        )
        # Keep predictions above the probability threshold
        above_df = k_df.filter(F.col('probability') > probability_threshold)
        # Concatenate to output dataframe
        output = output.union(above_df)

    return output


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description=(
            'Train & evaluate a Wikipedia section alignment model, then '
            'predict all (source, target) heading pairs'
        ),
    )

    # Required / positional
    parser.add_argument('snapshot', metavar='YYYY-MM-DD', help='snapshot date')

    # Optional
    parser.add_argument(
        '-w',
        '--work-dir',
        metavar='/hdfs_path/to/dir/',
        default=constants.WORK_DIR,
        help=f'Absolute HDFS path to the working directory. Default: "{constants.WORK_DIR}" in the current user home',
    )
    parser.add_argument(
        '-t',
        '--threshold',
        metavar='[0, 1]',
        default=PROBABILITY_THRESHOLD,
        help=f'Probability threshold between 0 and 1. Keep predictions above it. Default: {PROBABILITY_THRESHOLD}',
    )
    parser.add_argument(
        '-c',
        '--coalesce',
        metavar='N',
        default=constants.COALESCE,
        help=(
            'Control the amount of files per output partition. '
            'A higher value implies more files but a faster and lighter execution. '
            f'Default: {constants.COALESCE}'
        ),
    )

    return parser.parse_args()


def main(args: argparse.Namespace, spark: SparkSession) -> None:  # pragma: no cover
    snapshot = args.snapshot
    work_dir = args.work_dir
    threshold = args.threshold
    coalesce = args.coalesce

    training_path = constants.TRAINING_PATH.format(work_dir=work_dir)
    features_path = constants.FEATURES_PATH.format(work_dir=work_dir, snapshot=snapshot)
    models_path = constants.MODELS_PATH.format(work_dir=work_dir, snapshot=snapshot)
    alignments_path = constants.ALIGNMENTS_PATH.format(work_dir=work_dir, snapshot=snapshot)

    model = train(spark, features_path, training_path)
    # Save the locally trained model to a pickle in HDFS.
    # Inspired from XGBoost Spark model's `saveImpl` method:
    # https://github.com/dmlc/xgboost/blob/v1.7.6/python-package/xgboost/spark/model.py#L205
    # NOTE To load it back, use:
    #      model = spark.sparkContext.pickleFile(f'{models_path}/model.pkl').collect()[0]
    spark.sparkContext.parallelize([model], 1).saveAsPickleFile(f'{models_path}/model.pkl')

    precision_df = evaluate(model, spark, features_path, training_path)
    precision_df.write.csv(f'{models_path}/evaluation.csv', header=True)

    output = predict_all(model, spark, training_path, features_path, alignments_path, threshold)
    output = output.coalesce(coalesce)
    output.write.parquet(alignments_path, mode='overwrite')


if __name__ == '__main__':
    args = parse_args()

    # Uncomment for prod
    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_custom_session

    # spark = create_custom_session(
    # app_name='seal_model',
    # master='yarn',
    # ship_python_env=True,
    # spark_config={
    # # The model is trained locally, so all training data is pulled back to the driver
    # # NOTE These settings might make the driver unstable
    # # NOTE The production cluster's maximum allowed driver memory is 48g
    # 'spark.driver.memory': '46g',
    # 'spark.executor.memory': '8g',
    # 'spark.executor.cores': 4,
    # "spark.dynamicAllocation.maxExecutors": 128,
    # "spark.sql.shuffle.partitions": 512,
    # 'spark.executor.memoryOverhead': '4g',
    # 'spark.reducer.maxReqsInFlight': 1,
    # 'spark.shuffle.io.retryWait': '300s',
    # 'spark.driver.maxResultSize': 0,
    # # Mitigate `SparkException: Could not execute broadcast`:
    # # it may happen when pulling the training or test sets to the driver, see `train`.
    # "spark.sql.broadcastTimeout": "600s",
    # },
    # )
    main(args, spark)
    spark.stop()
