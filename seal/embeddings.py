#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Encode Wikipedia section heading embeddings with `LaBSE <https://tfhub.dev/google/LaBSE/2>`_,
a language-agnostic BERT sentence embedding model.

The pre-trained `PyTorch <https://pytorch.org/docs/2.0/>`_ model
must be available in the local file system at :const:`MODEL_PATH`.
You can download relevant files from
`Hugging Face <https://huggingface.co/sentence-transformers/LaBSE/tree/main>`_ through:

::

    from sentence_transformers import SentenceTransformer
    SentenceTransformer('sentence-transformers/LaBSE', cache_folder=MODEL_PATH)

Then package them into a tarball:

::

    $ cd MODEL_PATH
    $ tar cvzf ~/labse.tgz *

Finally, ship the tarball to Spark workers via the ``--archives`` option:

::

    $ spark3-submit ... --archives ~/labse.tgz#MODEL_PATH

The ``#MODEL_PATH`` suffix tells Spark to extract the tarball into that relative directory,
which will be picked up by this script.
"""

import argparse

from typing import Callable, Iterator, List, Tuple

from pyspark import RDD, Broadcast
from pyspark.ml import linalg
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from sentence_transformers import SentenceTransformer

from seal import constants


#: Relative path to the pre-trained LaBSE model files
MODEL_PATH = 'labse'


def extract_section_headings(
    section_attrs_df: DataFrame, existing_headings_df: DataFrame
) -> DataFrame:
    """Unpack section attributes to extract distinct section headings for each article
    the dataframe.
    """
    section_attrs_df = section_attrs_df.select(
        'wiki_db', F.explode('section_attributes').alias('section')
    )
    headings_df = section_attrs_df.withColumn('section', F.col('section.heading')).dropDuplicates()
    headings_df = headings_df.join(
        existing_headings_df, on=['wiki_db', 'section'], how='left_anti'
    )
    return headings_df


def encode_sections(
    encode: Broadcast,
) -> Callable[[RDD], Iterator[Tuple[str, List[float], str]]]:
    """Return a function that accepts RDDs and encodes all sections
    in it using the supplied encode method.
    """

    def encode_sections_inner(rows: RDD) -> Iterator[Tuple[str, List[float], str]]:
        wiki_db, sections = list(zip(*rows)) or ([], [])
        if not wiki_db or not sections:
            return
        embeddings = encode.value(sections, batch_size=128)
        for section, embedding, lang in zip(sections, embeddings, wiki_db):
            yield section, linalg.DenseVector(embedding), lang

    return encode_sections_inner


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description=(
            'Encode Wikipedia section heading embeddings with LaBSE: '
            'https://tfhub.dev/google/LaBSE/2'
        ),
    )

    # Required / positional
    parser.add_argument('snapshot', metavar='YYYY-MM-DD', help='current snapshot date')

    # Optional
    parser.add_argument(
        '-w',
        '--work-dir',
        metavar='/hdfs_path/to/dir/',
        default=constants.WORK_DIR,
        help=f'Absolute HDFS path to the working directory. Default: "{constants.WORK_DIR}" in the current user home',
    )
    parser.add_argument(
        '-p',
        '--previous-snapshot',
        metavar='YYYY-MM-DD',
        help=(
            'Previous snapshot of existing embeddings to avoid re-computation. ' 'Default: none'
        ),
    )
    parser.add_argument(
        '-c',
        '--coalesce',
        metavar='N',
        default=constants.EMBEDDINGS_COALESCE,
        help=(
            'Control the amount of files per output partition. '
            'A higher value implies more files but a faster and lighter execution. '
            f'Default: {constants.EMBEDDINGS_COALESCE}'
        ),
    )

    return parser.parse_args()


def main(args: argparse.Namespace, spark: SparkSession) -> None:  # pragma: no cover
    snapshot = args.snapshot
    previous_snapshot = args.previous_snapshot
    work_dir = args.work_dir
    coalesce = args.coalesce

    sections_path = constants.SECTIONS_PATH.format(work_dir=work_dir, snapshot=snapshot)
    embeddings_path = constants.EMBEDDINGS_PATH.format(work_dir=work_dir, snapshot=snapshot)
    if previous_snapshot is not None:
        existing_embeddings_path = constants.EMBEDDINGS_PATH.format(
            work_dir=work_dir, snapshot=previous_snapshot
        )
    else:
        existing_embeddings_path = None

    # Load a pre-trained model and explicitly broadcast
    # the `encode` method to all executors
    model = SentenceTransformer(MODEL_PATH)
    bc_encode = spark.sparkContext.broadcast(model.encode)

    if existing_embeddings_path is None:
        schema = T.StructType(
            [
                T.StructField('section', T.StringType(), False),
                T.StructField('embedding', linalg.VectorUDT(), False),
                T.StructField('wiki_db', T.StringType(), False),
            ]
        )
        existing_embeddings_df = spark.createDataFrame([], schema)
    else:
        existing_embeddings_df = spark.read.parquet(existing_embeddings_path)

    # Read parsed sections from HDFS for source and all targets
    sections_df = spark.read.parquet(sections_path)
    sections_df = extract_section_headings(
        sections_df, existing_embeddings_df.select('section', 'wiki_db')
    )

    # Instead of using a UDF to individually encode each section
    # we map the encode method to each equally sized partition. This
    # saves multiple function calls per section and results in significant
    # speed up despite the overhead of converting to rdd and back.
    # This also ensures that no one partition is skewed enough to be
    # a 'straggler'. Some variation between partitions is however unavoidable
    # because the time to encode varies from section to section.
    embeddings_df = (
        sections_df.rdd.repartition(1024)
        .mapPartitions(encode_sections(bc_encode))
        .toDF(['section', 'embedding', 'wiki_db'])
    )
    embeddings_df = embeddings_df.union(existing_embeddings_df)

    embeddings_df.coalesce(coalesce).write.parquet(embeddings_path, mode='overwrite')


if __name__ == '__main__':
    args = parse_args()

    # Uncomment for prod
    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_custom_session

    # spark = create_custom_session(
    # app_name='seal_embeddings',
    # master='yarn',
    # ship_python_env=True,
    # spark_config={
    # # Enough memory on the driver to cache
    # # and serialize the encoder.
    # 'spark.driver.memory': '8g',
    # # Enough memory on each executor to be able
    # # to retain the broadcasted encoder.
    # 'spark.executor.memory': '12g',
    # # Too many cores in the presence of a big
    # # broadcast variable would lead to the executor
    # # running out of memory.
    # 'spark.executor.cores': 2,
    # 'spark.dynamicAllocation.maxExecutors': 128,
    # 'spark.sql.shuffle.partitions': 512,
    # 'spark.executor.memoryOverhead': '6g',
    # },
    # )
    main(args, spark)
    spark.stop()
