#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Extract features from Wikipedia sections to train a section alignment model."""

import argparse
import json

from itertools import product
from typing import List

import pandas as pd

from pyspark.ml.linalg import DenseVector
from pyspark.sql import DataFrame, SparkSession, Window
from pyspark.sql import functions as F
from pyspark.sql import types as T

from seal import constants


SCHEMA = T.StructType(
    [
        T.StructField('source', T.StringType()),
        T.StructField('target', T.StringType()),
        T.StructField('source_links', T.ArrayType(T.StringType())),
        T.StructField('target_links', T.ArrayType(T.StringType())),
        T.StructField('source_count', T.IntegerType()),
        T.StructField('target_count', T.IntegerType()),
        T.StructField('source_pos_start', T.IntegerType()),
        T.StructField('target_pos_start', T.IntegerType()),
        T.StructField('source_pos_end', T.IntegerType()),
        T.StructField('target_pos_end', T.IntegerType()),
        T.StructField('wiki_db', T.StringType()),
    ]
)


def align_sections(source: str) -> F.pandas_udf:
    """Return a UDF that generates all possible combinations from source
    article sections to target article sections such that the source section
    for every pair would be in source language.
    """

    @F.pandas_udf(SCHEMA, F.PandasUDFType.GROUPED_MAP)
    def align_sections_inner(df: pd.DataFrame) -> pd.DataFrame:
        df['section_attributes'] = df['section_attributes'].map(json.loads)
        source_row = df[df['wiki_db'] == source]['section_attributes']
        target_rows = df[df['wiki_db'] != source][['section_attributes', 'wiki_db']].to_records(
            index=False
        )
        res = [
            (
                s1['heading'],
                s2['heading'],
                s1['links'],
                s2['links'],
                s1['count'],
                s2['count'],
                s1['pos_start_mean'],
                s2['pos_start_mean'],
                s1['pos_end_mean'],
                s2['pos_end_mean'],
                wiki_db,
            )
            for a, (b, wiki_db) in product(source_row, target_rows)
            for s1, s2 in product(a, b)
        ]
        columns = [
            'source',
            'target',
            'source_links',
            'target_links',
            'source_count',
            'target_count',
            'source_pos_start',
            'target_pos_start',
            'source_pos_end',
            'target_pos_end',
            'wiki_db',
        ]
        res_df = pd.DataFrame.from_records(res, columns=columns)
        return res_df

    return align_sections_inner


def generate_section_combinations(sections_df: DataFrame, source: str) -> DataFrame:
    # convert struct to json since Arrow does not allow nested structs
    sections_df = sections_df.withColumn('section_attributes', F.to_json('section_attributes'))
    sections_df = sections_df.groupby('item_id').apply(align_sections(source))
    return sections_df


def calculate_section_features(sections_df: DataFrame, drop: bool = True) -> DataFrame:
    """Returns a dataframe containing link_similarity_mean, link_similarity_sum,
    co-occurrence_count and Levenshtein distance columns. Drops columns with raw values
    after calculation if drop is True.
    """
    common_links = F.size(F.array_intersect('source_links', 'target_links'))
    all_links = F.size(F.array_union('source_links', 'target_links'))
    # calculate the Jaccard index for source and target sections
    features_df = sections_df.withColumn('link_similarity', common_links / all_links)
    features_df = features_df.groupBy(
        [
            'source',
            'target',
            'source_count',
            'target_count',
            'source_pos_start',
            'target_pos_start',
            'source_pos_end',
            'target_pos_end',
            'wiki_db',
        ]
    ).agg(
        F.count('*').alias('co-occurrence_count'),
        F.mean('link_similarity').alias('link_similarity_mean'),
        F.sum('link_similarity').alias('link_similarity_sum'),
    )

    w = Window.partitionBy('source', 'wiki_db')
    features_df = (
        features_df.withColumn('max-cooccurrence', F.max('co-occurrence_count').over(w))
        .withColumn(
            'co-occurrence_count_norm',
            F.col('co-occurrence_count') / F.col('max-cooccurrence'),
        )
        .drop('max-cooccurrence')
    )
    features_df = features_df.fillna(0, subset=['link_similarity_sum', 'link_similarity_mean'])
    if drop:
        features_df = features_df.drop('source_links', 'target_links')

    # calculate the levenshtein distance between source and target sections
    features_df = features_df.withColumn('edit_distance', F.levenshtein('source', 'target'))
    return features_df


def align_section_embeddings(
    sections_df: DataFrame, embeddings_df: DataFrame, top_sections: List[str]
) -> DataFrame:
    """Return dataframe containing embeddings for each source and target in ``sections_df``."""

    # In order to combat data skew during joins i.e., one section, such as references,
    # occurring a lot more times than others, we add a salt column. This adds a second
    # join condition which is more well distributed than the sections and thus prevents
    # all data going to a single executor. The trade-off is that we process more data
    # due to replication.
    # For sections_df, which is the larger dataframe, we put a random number next to
    # all occurrences of top_sections and a 0 next to the rest of the sections.
    salt_value = F.lit(F.rand() * 512 % 512).cast('int')
    sections_df = sections_df.withColumn(
        'source_salt', F.when(F.col('source').isin(top_sections), salt_value).otherwise(F.lit(0))
    ).withColumn(
        'target_salt', F.when(F.col('target').isin(top_sections), salt_value).otherwise(F.lit(0))
    )
    # For embeddings_df, we replicate the row containing a top_section 512 times (so salt
    #  is in the range 0 - 511) so that we can guarantee that one of these rows matches
    # the random number generated in the previous step. For the rest of the sections, we skip
    # replication and add a 0 like above.
    salt_col = F.explode(
        F.when(
            F.col('section').isin(top_sections), F.array([F.lit(i) for i in range(512)])
        ).otherwise(F.array(F.lit(0)))
    )
    embeddings_df = embeddings_df.select('*', salt_col.alias('salt'))

    # conditions for aligning source embeddings
    source_and_section_match = F.col('sections.source') == F.col('e1.section')
    source_salt_and_salt_match = F.col('sections.source_salt') == F.col('e1.salt')
    # conditions for aligning target embeddings
    target_and_section_match = F.col('sections.target') == F.col('e2.section')
    target_salt_and_salt_match = F.col('sections.target_salt') == F.col('e2.salt')
    sections_df = (
        sections_df.alias('sections')
        .join(embeddings_df.alias('e1'), source_salt_and_salt_match & source_and_section_match)
        .join(embeddings_df.alias('e2'), target_salt_and_salt_match & target_and_section_match)
        .select(
            'sections.*',
            F.col('e1.embedding').alias('source_embedding'),
            F.col('e2.embedding').alias('target_embedding'),
        )
    )
    sections_df = sections_df.drop('source_salt', 'target_salt')
    return sections_df


@F.udf(returnType=T.FloatType())
def dot(v1: DenseVector, v2: DenseVector) -> float:
    return float(v1.dot(v2))


def calculate_section_similarity(sections_df: DataFrame, drop: bool = True) -> DataFrame:
    """Calculates cosine similarity between source and target embeddings. Drops columns with raw values
    after calculation if drop is True.
    """
    # Since the embeddings are already L2 normalized, computing the cosine
    # similarity is equivalent to the dot product.
    sections_df = sections_df.withColumn(
        'heading_similarity', dot(F.col('source_embedding'), F.col('target_embedding'))
    )
    if drop:
        sections_df = sections_df.drop('source_embedding', 'target_embedding')
    return sections_df


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Extract features from Wikipedia sections to train a section alignment model'
    )

    # Required / positional
    parser.add_argument('snapshot', metavar='YYYY-MM-DD', help='current snapshot date')

    # Optional
    parser.add_argument(
        '-w',
        '--work-dir',
        metavar='/hdfs_path/to/dir/',
        default=constants.WORK_DIR,
        help=f'Absolute HDFS path to the working directory. Default: "{constants.WORK_DIR}" in the current user home',
    )
    parser.add_argument(
        '-r',
        '--repartition',
        type=int,
        metavar='N',
        default=constants.FEATURES_REPARTITION,
        help=f'Control the amount of output files. Default: {constants.FEATURES_REPARTITION}',
    )

    return parser.parse_args()


def main(args: argparse.Namespace, spark: SparkSession) -> None:  # pragma: no cover
    snapshot = args.snapshot
    work_dir = args.work_dir
    repartition = args.repartition

    training_path = constants.TRAINING_PATH.format(work_dir=work_dir)
    embeddings_path = constants.EMBEDDINGS_PATH.format(work_dir=work_dir, snapshot=snapshot)
    sections_path = constants.SECTIONS_PATH.format(work_dir=work_dir, snapshot=snapshot)
    features_path = constants.FEATURES_PATH.format(work_dir=work_dir, snapshot=snapshot)

    # TODO do training resources need to be regularly generated?
    cx_sources = spark.read.json(f'{training_path}/content_translation_sources.json').collect()
    langs = [s['wp-code'] for s in cx_sources]

    # Read embeddings from HDFS for source and all targets
    embeddings_df = spark.read.parquet(embeddings_path)
    embeddings_df = embeddings_df.select('section', 'embedding').dropDuplicates(['section'])

    # Read parsed sections from HDFS for source and all targets
    sections_df = spark.read.parquet(sections_path)

    # Read top sections across major wikis from HDFS
    # TODO do training resources need to be regularly generated?
    top_section_rows = spark.read.json(f'{training_path}/top_sections.json').collect()
    top_sections = [row['section'] for row in top_section_rows]

    for lang in langs:
        source = f"{lang.replace('-', '_')}wiki"
        section_combinations_df = generate_section_combinations(sections_df, source)

        # Group all matching (source, target) pairs and calculate features
        features_df = calculate_section_features(section_combinations_df)

        # Align embeddings once we no longer have duplicate pairs to reduce work
        features_df = align_section_embeddings(features_df, embeddings_df, top_sections)
        features_df = calculate_section_similarity(features_df)
        features_df = features_df.withColumn('source_language', F.lit(source)).withColumnRenamed(
            'wiki_db', 'target_language'
        )

        features_df.repartition(repartition).write.parquet(
            f'{features_path}/{source}', mode='overwrite'
        )


if __name__ == '__main__':
    args = parse_args()

    # Uncomment for prod
    spark = SparkSession.builder.getOrCreate()
    # Uncomment for dev
    # from wmfdata.spark import create_custom_session

    # spark = create_custom_session(
    # app_name='seal_features',
    # master='yarn',
    # ship_python_env=True,
    # # https://spark.apache.org/docs/3.1.2/configuration.html
    # # https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Extra_large_jobs
    # spark_config={
    # # Increase resources
    # 'spark.driver.memory': '32g',
    # 'spark.executor.memory': '12g',
    # 'spark.executor.cores': 4,
    # "spark.dynamicAllocation.maxExecutors": 128,
    # 'spark.sql.shuffle.partitions': 4096,
    # 'spark.executor.memoryOverhead': '6g',
    # # Mitigate `FetchFailed` due to `Timeout`
    # 'spark.shuffle.service.index.cache.size': 2048,
    # # TODO if the above doesn't fix fetch failures, try spark.shuffle.io.backLog
    # 'spark.stage.maxConsecutiveAttempts': 10,
    # 'spark.reducer.maxReqsInFlight': 1,
    # 'spark.shuffle.io.maxRetries': 10,
    # 'spark.shuffle.io.retryWait': '60s',
    # },
    # )
    main(args, spark)
    spark.stop()
