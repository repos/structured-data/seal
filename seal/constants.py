#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A set of constants used across modules."""

WORK_DIR = 'seal'

# To be formatted at runtime
TRAINING_PATH = '{work_dir}/training'
EMBEDDINGS_PATH = '{work_dir}/embeddings/{snapshot}'
SECTIONS_PATH = '{work_dir}/sections/{snapshot}'
FEATURES_PATH = '{work_dir}/features/{snapshot}'
MODELS_PATH = '{work_dir}/models/{snapshot}'
ALIGNMENTS_PATH = '{work_dir}/alignments/{snapshot}'

# Control the amount of files per output partition.
# A higher value implies more files but a faster and lighter execution.
COALESCE = 8
# Embeddings' output size with no `coalesce` is 1,025.
# The default `coalesce` value breaks the execution, so
# decrease the size by an order of magnitude.
EMBEDDINGS_COALESCE = 100
# `coalesce` impairs feature extraction's scalability and
# the output size is relatively small, so we use `repartition`.
# Usual output files with no `repartition` amount to
# roughly 3.9k per directory: 807k files across 207 directories (read wikis).
# Decrease the size by 3 orders of magnitude circa.
FEATURES_REPARTITION = 4
